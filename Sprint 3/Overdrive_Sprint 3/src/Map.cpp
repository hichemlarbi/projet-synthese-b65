#include "Map.h"

using namespace std;

Map::Map(string imgFile){
    //Buffers
    bufferMap();

    this->imgFile = imgFile;
    loadImage(imgFile);
    setTexture(imgFile);

}
int Map::bufferMap(){
    /*Sending the shape to the buffer*/

    float vertices[] = {
        //position       //texture
        -1.0, -1.0, 0.0, 0.0, 0.0,
         1.0, -1.0, 0.0, 1.0, 0.0,
         1.0,  1.0, 0.0, 1.0, 1.0,
         1.0,  1.0, 0.0, 1.0, 1.0,
        -1.0,  1.0, 0.0, 0.0, 1.0,
        -1.0, -1.0, 0.0, 0.0, 0.0
    };

    /******************** BUFFERS ********************/
     /***Vertex Array***/
    glGenVertexArrays(1, &this->arrayId);
    glBindVertexArray(this->arrayId);

    /***Vertex Buffer***/
    glGenBuffers(1, &this->bufferId);
    glBindBuffer(GL_ARRAY_BUFFER, this->bufferId);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

    /***Interpretation***/
    /***location, size (vecX), type of elements, bool normalized, stride(space between sets), offset***/
    // position attribute
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)0);
    glEnableVertexAttribArray(0);
    // color attribute
    //glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)(3 * sizeof(float)));
    //glEnableVertexAttribArray(1);
    // texture attribute
    glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)(3 * sizeof(float)));
    glEnableVertexAttribArray(2);

    //bindingIds
    return arrayId;
}

sf::Image Map::loadImage(string path){
    sf::Image texture;
    if(!texture.loadFromFile(path)){
        cout << "Failed to load texture " << path << endl;
    }
    return texture;
}


/***Getters & Setters***/
/*----------Getters----------*/
glm::mat4 Map::getTransformation(){
    return model;
}
int Map::getTextureHandler(){
    return textureHandler;
}
int Map::getArrayId(){
    return arrayId;
}
int Map::getShaderId(){
    return shaderId;
}
glm::mat4 Map::getModel(){
    return model;
}

/*----------Setters----------*/
void Map::setModel(glm::mat4 model){
    this->model = model;
}


void Map::setTexture(string imgFile){
    /***Loading textures***/
    texture = loadImage(imgFile);
    //Texture object
    glGenTextures(1, &textureHandler);
    glBindTexture(GL_TEXTURE_2D, textureHandler);

    //image wraping if going out of bounds
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    /***Mipmap***/
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST); //magnification does not use mipmapping

    //Upload image to GPU
    glTexImage2D(
        GL_TEXTURE_2D, 0, GL_RGBA,
        texture.getSize().x, texture.getSize().y, 0,
        GL_RGBA, GL_UNSIGNED_BYTE, texture.getPixelsPtr()
    );
}
