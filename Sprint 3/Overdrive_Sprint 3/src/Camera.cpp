#include "Camera.h"

using namespace std;

Camera::Camera(glm::vec3 camPos, glm::vec3 camFront, glm::vec3 camUp, float speed, float pitch, float yaw, float intensity){
    this->cameraPos = camPos;
    this->cameraFront = camFront;
    this->cameraUp = camUp;
    this->speed = speed;
    this->pitch = pitch;
    this->yaw = yaw;
    this->intensity = intensity;

}

void Camera::moveCam(){

    /***Keyboard Input***/
    if(sf::Keyboard::isKeyPressed(sf::Keyboard::W) || sf::Joystick::isButtonPressed(controllerID, Y))
        isAccelerating = true;

    else if(sf::Keyboard::isKeyPressed(sf::Keyboard::S) || sf::Joystick::isButtonPressed(controllerID, B))
        isReversing = true;

    else{
        isAccelerating = false;
        isReversing = false;
    }

    /*pitch & Yaw*/
    /*if(sf::Keyboard::isKeyPressed(sf::Keyboard::Up))
        if(pitch < 89)
            pitch += intensity;

    if(sf::Keyboard::isKeyPressed(sf::Keyboard::Down))
        if(pitch > -89)
            pitch -= intensity;*/

    if(sf::Keyboard::isKeyPressed(sf::Keyboard::Left) || sf::Joystick::getAxisPosition(controllerID, sf::Joystick::X) == LEFT){
        isTurningLeft = true;
        yaw -= intensity;
        //Handbrake
        if(sf::Keyboard::isKeyPressed(sf::Keyboard::Space) || sf::Joystick::isButtonPressed(controllerID, L) || sf::Joystick::isButtonPressed(controllerID, R))
            isDrifting = true;
        else
            isDrifting = false;
    }

    else if(sf::Keyboard::isKeyPressed(sf::Keyboard::Right) || sf::Joystick::getAxisPosition(controllerID, sf::Joystick::X) == RIGHT){
        isTurningRight = true;
        yaw += intensity;
            //Handbrake
        if(sf::Keyboard::isKeyPressed(sf::Keyboard::Space) || sf::Joystick::isButtonPressed(controllerID, L) || sf::Joystick::isButtonPressed(controllerID, R))
            isDrifting = true;
        else
            isDrifting = false;
    }
    else{
        isTurningLeft = false;
        isTurningRight = false;
    }
}

void Camera::updateCam(){
    /***Update Right Vector***/
    this->cameraRight = glm::normalize(glm::cross(cameraUp, cameraFront));

    /***Update Pitch and Yaw***/
    //convertir pitch et yaw en coord 3D
    cameraFront.x = cos(glm::radians(pitch)) * cos(glm::radians(yaw));
    cameraFront.y = sin(glm::radians(pitch));
    cameraFront.z = cos(glm::radians(pitch)) * sin(glm::radians(yaw));

    cameraFront = glm::normalize(cameraFront);

    //std::cout << "left = " << isTurningLeft << std::endl;
    //std::cout << "right = " << isTurningRight << std::endl;
}

/***View & Projection Matrix***/
glm::mat4 Camera::getModel(){
    return model;
}
glm::mat4 Camera::getView(){
    return view;
}
glm::mat4 Camera::getProjection(){
    return projection;
}
void Camera::setModel(glm::mat4 model){
    this->model = model;
}
void Camera::setView(glm::mat4 view){
    this->view = view;
}
void Camera::setProjection(glm::mat4 projection){
    this->projection = projection;
}



/***Getters & Setters***/
glm::vec3 Camera::getCameraPos(){
    return cameraPos;
}
glm::vec3 Camera::getCameraFront(){
    return cameraFront;
}
glm::vec3 Camera::getCameraUp(){
    return cameraUp;
}
glm::vec3 Camera::getCameraRight(){
    return cameraRight;
}
float Camera::getSpeed(){
    return speed;
}
float Camera::getPitch(){
    return pitch;
}
float Camera::getYaw(){
    return yaw;
}
float Camera::getIntensity(){
    return intensity;
}
glm::mat4 Camera::getViewMat(){
    return glm::lookAt(cameraPos, cameraPos + cameraFront, cameraUp);
}
bool Camera::getTurningRight(){
    return isTurningRight;
}
bool Camera::getTurningLeft(){
    return isTurningLeft;
}
bool Camera::getAccelerating(){
    return isAccelerating;
}
bool Camera::getReversing(){
    return isReversing;
}
bool Camera::getDrifting(){
    return isDrifting;
}

void Camera::setCameraPos(glm::vec3 n_cameraPos){
    cameraPos = n_cameraPos;
}
void Camera::setCameraFront(glm::vec3 n_cameraFront){
    cameraFront = n_cameraFront;
}
void Camera::setCameraUp(glm::vec3 n_cameraUp){
    cameraUp = n_cameraUp;
}
void Camera::setSpeed(float n_speed){
    speed = n_speed;
}
void Camera::setPitch(float n_pitch){
    pitch = n_pitch;
}
void Camera::setYaw(float n_yaw){
    yaw = n_yaw;
}
void Camera::setIntensity(float n_intensity){
    intensity = n_intensity;
}
