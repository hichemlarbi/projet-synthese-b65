#include "FixedObjects.h"

FixedObjects::FixedObjects(Camera *cam, float screen_w, float screen_h, glm::vec3 pos, std::string texPath, int texWidth, int texHeight, const GLchar* vs, const GLchar* fs)
        :MakeQuad(cam, screen_w, screen_h, texPath, texWidth, texHeight, vs, fs) {
    //ctor
    //Normalisé les coordonnées réels et scaler par la taille du terrain
    //this->pos = (glm::normalize(pos)) * 20.0f;
    this->pos = pos;
}

void FixedObjects::event(){}

void FixedObjects::update(){}

void FixedObjects::draw(){
    /**************************************************
        Méthode pour dessiner la forme selon les
        données voulu
    ***************************************************/
    //Draw
    this->shader.use(); //use the shader

    /***Chercher la bonne texture et le binder***/
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, textureHandler);
    this->shader.setInt("ourTexture", 0);
    /***Chercher l'array et le binder***/
    glBindVertexArray(arrayId); //bind array
    //Envoi du billboarding au shader
    outilBillboard.makeBillboard(this->shader, this->cam, pos, rotation, scale);
    //outilBillboard.orthoBillboard(this->shader, this->cam, pos, rotation, scale);
    /***Dessiner***/
    glDrawArrays(GL_TRIANGLES, 0, 36); //draw all binded arrays of triangles
    /***Unbind l'array***/
    glBindVertexArray(0);
}
void FixedObjects::run(){
    this->event();
    this->update();
    this->draw();
}

