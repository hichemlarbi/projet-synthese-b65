#include "SkyBox.h"

SkyBox::SkyBox(Camera* cam){
    this->cam = cam;
    this->cubeMap = CubeMap();
    this->cubeMapTexture = this->cubeMap.loadCubeMap(this->faces);
}

int SkyBox::loadData(){

    /***Coords***/
    float skyboxVertices[] = {
        // positions
        -1.0f,  1.0f, -1.0f,
        -1.0f, -1.0f, -1.0f,
         1.0f, -1.0f, -1.0f,
         1.0f, -1.0f, -1.0f,
         1.0f,  1.0f, -1.0f,
        -1.0f,  1.0f, -1.0f,

        -1.0f, -1.0f,  1.0f,
        -1.0f, -1.0f, -1.0f,
        -1.0f,  1.0f, -1.0f,
        -1.0f,  1.0f, -1.0f,
        -1.0f,  1.0f,  1.0f,
        -1.0f, -1.0f,  1.0f,

         1.0f, -1.0f, -1.0f,
         1.0f, -1.0f,  1.0f,
         1.0f,  1.0f,  1.0f,
         1.0f,  1.0f,  1.0f,
         1.0f,  1.0f, -1.0f,
         1.0f, -1.0f, -1.0f,

        -1.0f, -1.0f,  1.0f,
        -1.0f,  1.0f,  1.0f,
         1.0f,  1.0f,  1.0f,
         1.0f,  1.0f,  1.0f,
         1.0f, -1.0f,  1.0f,
        -1.0f, -1.0f,  1.0f,

        -1.0f,  1.0f, -1.0f,
         1.0f,  1.0f, -1.0f,
         1.0f,  1.0f,  1.0f,
         1.0f,  1.0f,  1.0f,
        -1.0f,  1.0f,  1.0f,
        -1.0f,  1.0f, -1.0f,

        -1.0f, -1.0f, -1.0f,
        -1.0f, -1.0f,  1.0f,
         1.0f, -1.0f, -1.0f,
         1.0f, -1.0f, -1.0f,
        -1.0f, -1.0f,  1.0f,
         1.0f, -1.0f,  1.0f
    };

    /******************** BUFFERS ********************/
    /***Vertex Array***/
    unsigned int arrayId;
    glGenVertexArrays(1, &arrayId);
    glBindVertexArray(arrayId);

    /***Vertex Buffer***/
    unsigned int bufferId;
    glGenBuffers(1, &bufferId);
    glBindBuffer(GL_ARRAY_BUFFER, bufferId);
    glBufferData(GL_ARRAY_BUFFER, sizeof(skyboxVertices), skyboxVertices, GL_STATIC_DRAW);

    /***Interpretation***/
    /***location, size (vecX), type of elements, bool normalized, stride(space between sets), offset***/
    // position attribute
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*)0);
    glEnableVertexAttribArray(0);

    //bindingIds
    return arrayId;

}

void SkyBox::draw(int shaderId){
    //remove translation
    glm::mat4 view = glm::mat4(glm::mat3(cam->getViewMat()));

    /***envoi de la matrice de transformation au shader***/
    /*Vue*/
    int viewLocation = glGetUniformLocation(shaderId, "vue");
    glUniformMatrix4fv(viewLocation ,1,GL_FALSE, glm::value_ptr(view));
    //glUniformMatrix4fv(viewLocation ,1,GL_FALSE, glm::value_ptr(cam->getView()));
    /*Projection*/
    int projLocation = glGetUniformLocation(shaderId, "projection");
    glUniformMatrix4fv(projLocation ,1,GL_FALSE, glm::value_ptr(cam->getProjection()));


}

/***Getters & Setters***/
int SkyBox::getTextureData(){
    return cubeMapTexture;
}






