#include "GameLogic.h"

GameLogic::GameLogic() : myCam(glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(0.0f, 0.0f, -1.0f), glm::vec3(0.0f, 1.0f, 0.0f), 0.1, 0.0, 0.0, 1),
                         muteCity(&myCam, window.getSize().x, window.getSize().y, "mute-city.png", "assets/mute-city_collision.png", glm::vec3(-8.0f, 0.0f, -14.0f), 6880, 3488, "Shaders/Vertex/vertexShader.vert", "Shaders/Fragment/fragmentShader.frag"),
                         laps(&myCam, muteCity, window.getSize().x, window.getSize().y, "assets/numbers.png",500, 50, "Shaders/Vertex/texteVertex.vert", "Shaders/Fragment/texteFragment.frag"),
                         blueFalcon(&myCam, muteCity, hud, &timer, window.getSize().x, window.getSize().y, "assets/outrun.png",360, 30, "Shaders/Vertex/vehicleVertex.vert", "Shaders/Fragment/vehicleFragment.frag"),
                         skyBox(&myCam),
                         window(sf::VideoMode(800, 600), "Overdrive", sf::Style::Close, settings){


    std::cout << "initialisation gamelogic" << std::endl;
    seconds = sf::seconds(10.0f);
    /***Camera***/
    //             glm::vec3 camPos,              glm::vec3 camFront,           glm::vec3 camUp,             speed,pitch,yaw,intensity
    //myCam = Camera(glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(0.0f, 0.0f, -1.0f), glm::vec3(0.0f, 1.0f, 0.0f), 0.1, 0.0, 0.0, 1);

    /*Viewport*/
    settings.depthBits = 24;
    settings.stencilBits = 8;
    settings.antialiasingLevel = 3; // Optional
    // Request OpenGL version 3.2 (optional but recommended)
    settings.majorVersion = 3;
    settings.minorVersion = 3;
    settings.attributeFlags = sf::ContextSettings::Core;

    window.setFramerateLimit(60);
    /***Initializing openGL***/
    glewExperimental = GL_TRUE;
    glewInit();

    //Coordonnees de la texture de la map
    blueFalcon.getTexCoord(muteCity);


    /***DEPTH BUFFER***/
    // Enable depth test
    glEnable(GL_DEPTH_TEST);
    //Enable blending (pour la transparence)
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);
    // Accept fragment if it closer to the camera than the former one
    //glDepthFunc(GL_LESS);

    /***** BUFFERS *****/
    //Skybox
    sb_arrayId = skyBox.loadData();
    sb_texHandler = skyBox.getTextureData();

    /***Avec la class Shader***/
    skyBoxShader = CreateShader("Shaders/Vertex/skyBoxShader.vert", "Shaders/Fragment/skyBoxShader.frag");

    /****************************************************/
    /***Test de création d'un track avec la class Quad***/
    /****************************************************/
    muteCity.createQuad();
    blueFalcon.createQuad();
}

void GameLogic::event(){}

void GameLogic::update(){
    myCam.moveCam();
    myCam.updateCam();
}

void GameLogic::draw(){
    muteCity.draw();
}

void GameLogic::run(){
    event();
    update();
    draw();
}

void GameLogic::mainLoop(){
    while (running){

        float currentTime = clock.restart().asSeconds();
        float fps = 1.0f / currentTime;
        blueFalcon.setCurTime(currentTime);
        //std::cout << "fps: " << fps << std::endl;

        /***Timer***/
        //std::cout << "secs : " << seconds.asSeconds() << std::endl;
        if(timer.getElapsedTime().asSeconds() >= seconds.asSeconds())
            timer.restart().asSeconds();
        //std::cout << "time : " << timer.getElapsedTime().asSeconds() << std::endl;


        // Process events
        sf::Event event;
        while (window.pollEvent(event))
        {
            // Close window : exit
            if (event.type == sf::Event::Closed)
                running = false;
        }
        //window.setActive(true);

        /***Clear Buffers***/
        glClearColor(0.0f, 0.0f, 0.2f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        /***Draw Skybox***/
        glDepthMask(GL_FALSE);
        skyBoxShader.use();
        skyBox.draw(skyBoxShader.id);
        glBindVertexArray(sb_arrayId);
        glBindTexture(GL_TEXTURE_CUBE_MAP, sb_texHandler);
        glDrawArrays(GL_TRIANGLES, 0, 36);
        glDepthMask(GL_TRUE);

        /***Event***/
        //this.event();
        /***Update***/
        update();
        /***Draw***/
        draw();
        /***Run***/
        blueFalcon.run();
        window.display();
    }

}






