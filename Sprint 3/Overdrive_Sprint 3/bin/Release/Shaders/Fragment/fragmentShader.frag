#version 330 core

out vec4 FragColor; //variable to set color
in vec3 myColor; //Get color from vertex shader

in vec2 TexCoord;

//uniform vec4 myColor;
uniform sampler2D ourTexture;

float near = 0.1;
float far  = 2.0; // 0 = aucun fog, proche de 0 = fog intense

float LinearizeDepth(float depth)
{
    float z = depth * 2.0 - 1.0; // back to NDC
    return (2.0 * near * far) / (far + near - z * (far - near));
}

void main()
{

    //FragColor = texture(ourTexture, TexCoord);
    //Fog
    vec3 color = texture(ourTexture, TexCoord).rgb;
    float depth = LinearizeDepth(gl_FragCoord.z) / far;
    vec3 colorfog = vec3(0.2,0.0,0.2); //meme couleur que dans la skybox

    FragColor = vec4(mix(color,colorfog, depth), 1.0);

}
