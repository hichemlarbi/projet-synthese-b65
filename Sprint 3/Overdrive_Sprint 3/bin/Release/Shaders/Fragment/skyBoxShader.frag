#version 330 core

out vec4 FragColor; //variable to set color
in vec3 TexCoords; //Get color from vertex shader

uniform samplerCube skybox;

const float lowerLimite = -0.0;
const float upperLimite = 1.0;

void main()
{
    vec3 color = texture(skybox, TexCoords).rgb;
    vec3 colorSun = vec3(0.4,0.0,0.4);
    vec3 colorMoon = vec3(0.0,0.0,0.2);

    float fogIntensity = 0.6; //0 = intense, 1 = pas de fog

    float factor = (TexCoords.y - lowerLimite) / (upperLimite - lowerLimite);
    factor = clamp(factor, 0.0, 1.0);

    vec3 skycolor = mix(colorSun, colorMoon, factor);

    FragColor = vec4(mix(skycolor, color, fogIntensity), 1.0);
}
