#ifndef SKYBOX_H
#define SKYBOX_H

#include <SFML/Graphics.hpp>
#define GLEW_STATC
#include <GL/glew.h>
#include <iostream>
#include <math.h>
#include <string.h>
/***extern class***/
#include "Camera.h"
#include "CubeMap.h"
/***OpenGL Maths***/
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>


class SkyBox
{
    public:
        SkyBox(Camera* cam);
        int loadData();
        int getTextureData();
        void draw(int shaderId);

    protected:

    private:
        CubeMap cubeMap;
        Camera *cam;
        std::vector<std::string> faces{
            "skybox_mtl/right.png",
            "skybox_mtl/left.png",
            "skybox_mtl/top.png",
            "skybox_mtl/bottom.png",
            "skybox_mtl/front.png",
            "skybox_mtl/back.png"
        };
        unsigned int cubeMapTexture;

};

#endif // SKYBOX_H
