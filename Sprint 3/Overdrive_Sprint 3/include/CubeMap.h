#ifndef CUBEMAP_H
#define CUBEMAP_H

#include <SFML/Graphics.hpp>
#define GLEW_STATC
#include <GL/glew.h>
#include <iostream>
#include <math.h>
#include <string.h>
/***extern class***/
#include "Camera.h"
/***OpenGL Maths***/
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

class CubeMap
{
    public:
        CubeMap();
        virtual ~CubeMap();

        sf::Image loadImage(std::string path);
        int loadCubeMap(std::vector<std::string> faces);


    private:
        sf::Image texture;
};

#endif // CUBEMAP_H
