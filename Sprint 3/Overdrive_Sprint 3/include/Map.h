#ifndef MAP_H
#define MAP_H

#include <SFML/Graphics.hpp>
#define GLEW_STATC
#include <GL/glew.h>
#include <iostream>
#include <math.h>
#include <string.h>
/***openGL Maths***/
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

class Map
{
    public:
        Map(std::string imgfile);
        int bufferMap();
        sf::Image loadImage(std::string path);

        /***Getters & Setters***/
        glm::mat4 getTransformation();
        int getTextureHandler();
        int getArrayId();
        int getShaderId();
        float getVertices();
        glm::mat4 getModel();

        void setScale(glm::vec3 scaleFactor);
        void setTexture(std::string imgFile);
        void setShaderId(int shaderId);
        void setModel(glm::mat4 model);


    private:
        glm::mat4 model;
        GLuint textureHandler;
        sf::Image texture;
        unsigned int bufferId;
        unsigned int arrayId;
        int shaderId;
        std::string imgFile;

};

#endif // MAP_H
