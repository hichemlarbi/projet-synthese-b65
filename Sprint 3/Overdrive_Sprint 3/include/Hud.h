#ifndef HUD_H
#define HUD_H

#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#define GLEW_STATC
#include <GL/glew.h>
#include <iostream>
#include <math.h>
#include <string.h>
/***extern class***/
#include "Camera.h"
#include "MakeQuad.h"
#include "Track.h"
#include "Billboard.h"
#include "Texte.h"
#include "Hud.h"

/***OpenGL Maths***/
#include <glm/ext.hpp>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>


class Hud{
    public:
        Hud(Texte *lapMilisec, Texte *lapSec, Texte *lapSecDizaine, Texte *lapMinute, Texte *lapMinuteDizaine, Texte *minSeparator, Texte *secSeparator,
            Texte *recordLapMilisec, Texte *recordLapSec, Texte *recordLapSecDizaine, Texte *recordLapMinute, Texte *recordLapMinuteDizaine, Texte *recordMinSeparator, Texte *recordSecSeparator);
        void event();
        void update();
        void draw();
        void run();

        void resetChrono();

        void setLapTime(int *lapTime);
        int *getLapTime();
        void setBestLapTime();

        /***Getters & Setters***/
        void setChrono(glm::vec2);


    protected:

    private:
        /***current time***/
        //numbers
        Texte *lapMilisec;
        Texte *lapSec;
        Texte *lapSecDizaine;
        Texte *lapMinute;
        Texte *lapMinuteDizaine;
        //separators
        Texte *minSeparator;
        Texte *secSeparator;
        /***record***/
        //numbers
        Texte *recordLapMilisec;
        Texte *recordLapSec;
        Texte *recordLapSecDizaine;
        Texte *recordLapMinute;
        Texte *recordLapMinuteDizaine;
        //separators
        Texte *recordMinSeparator;
        Texte *recordSecSeparator;

        int curLapTime[5]{0, 0, 0, 0, 0};
        int bestLapTime[5]{0, 5, 2, 1, 0};

        glm::vec2 chrono{0, 0};
        int secondeDizaine{0};
        int minute{0};
        int minuteDizaine{0};

};

#endif // HUD_H
