#ifndef FIXEDOBJECTS_H
#define FIXEDOBJECTS_H

#include <SFML/Graphics.hpp>
#define GLEW_STATC
#include <GL/glew.h>
#include <iostream>
#include <math.h>
#include <string.h>
/***extern class***/
#include "Camera.h"
#include "MakeQuad.h"
#include "Track.h"
#include "Billboard.h"

/***OpenGL Maths***/
#include <glm/ext.hpp>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>


class FixedObjects : public MakeQuad{
    public:
        FixedObjects(Camera *cam, float screen_w, float screen_h, glm::vec3 pos, std::string texPath, int texWidth, int texHeight, const GLchar* vs, const GLchar* fs);

        void event();
        void update();
        void draw();
        void run();

    private:
        /*Variables du vehicule*/
        glm::vec3 pos{0.0f, 0.0f, 0.0f};

        /***REVOIR LES OFFSETS POUR LE TEXTE***/
        float rotation{90.0f};
        float scale{3.0f};
        glm::vec3 offset{0.5f, 0.2f, -0.25f};

        /*Autres variables*/
        Billboard outilBillboard;
};

#endif // FIXEDOBJECTS_H
