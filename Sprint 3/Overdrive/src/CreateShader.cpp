#include "CreateShader.h"

using namespace std;

//default ctor
CreateShader::CreateShader(){}

CreateShader::CreateShader(const GLchar* vertexPath,const GLchar* fragmentPath){
    // 1. retrieve the vertex/fragment source code from filePath
    string vertexCode;
    string fragmentCode;
    ifstream vShaderFile;
    ifstream fShaderFile;
    // ensure ifstream objects can throw exceptions:
    vShaderFile.exceptions (ifstream::failbit | ifstream::badbit);
    fShaderFile.exceptions (ifstream::failbit | ifstream::badbit);
    try
    {
        // open files
        vShaderFile.open(vertexPath);
        fShaderFile.open(fragmentPath);
        stringstream vShaderStream, fShaderStream;
        // read file's buffer contents into streams
        vShaderStream << vShaderFile.rdbuf();
        fShaderStream << fShaderFile.rdbuf();
        // close file handlers
        vShaderFile.close();
        fShaderFile.close();
        // convert stream into string
        vertexCode   = vShaderStream.str();
        fragmentCode = fShaderStream.str();
    }
    catch (ifstream::failure e)
    {
        cout << "ERROR::SHADER::FILE_NOT_SUCCESFULLY_READ" << endl;
    }

    const char* vShaderCode = vertexCode.c_str();
    const char* fShaderCode = fragmentCode.c_str();

    /******************** SHADERS ********************/
    /***Vertex Shader***/
    int vShader;
    vShader = glCreateShader(GL_VERTEX_SHADER);
    glShaderSource(vShader, 1, &vShaderCode, NULL);
    glCompileShader(vShader);
    // check for shader compile errors
    int success;
    char infoLog[512];
    glGetShaderiv(vShader, GL_COMPILE_STATUS, &success);
    if (!success)
    {
        glGetShaderInfoLog(vShader, 512, NULL, infoLog);
        std::cout << "ERROR::SHADER::VERTEX::COMPILATION_FAILED\n" << infoLog << std::endl;
    }

    /***Fragment Shader***/
    int fShader;
    fShader = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(fShader, 1, &fShaderCode, NULL);
    glCompileShader(fShader);
     // check for shader compile errors
    glGetShaderiv(fShader, GL_COMPILE_STATUS, &success);
    if (!success)
    {
        glGetShaderInfoLog(fShader, 512, NULL, infoLog);
        std::cout << "ERROR::SHADER::FRAGMENT::COMPILATION_FAILED\n" << infoLog << std::endl;
    }

    /******************** LINKING ********************/
    /***Shader Program***/
    id = glCreateProgram(); //REPRENDRE ID ICI
    glAttachShader(id, vShader);
    glAttachShader(id, fShader);
    glLinkProgram(id);

    glGetProgramiv(id, GL_LINK_STATUS, &success);
    if (!success) {
        glGetProgramInfoLog(id, 512, NULL, infoLog);
        std::cout << "ERROR::SHADER::PROGRAM::LINKING_FAILED\n" << infoLog << std::endl;
    }
    /***DELETING SHADERS***/
    glDeleteShader(vShader);
    glDeleteShader(fShader);
}


void CreateShader::use(){
    glUseProgram(id);
}

void CreateShader::setBool(const string &name, bool value) const{
    glUniform1i(glGetUniformLocation(id, name.c_str()), (int)value);
}
// ------------------------------------------------------------------------
void CreateShader::setInt(const string &name, int value) const{
    glUniform1i(glGetUniformLocation(id, name.c_str()), value);
}
// ------------------------------------------------------------------------
void CreateShader::setFloat(const string &name, float value) const{
    glUniform1f(glGetUniformLocation(id, name.c_str()), value);
}
void CreateShader::setVec2(const string &name, const glm::vec2 &value) const{
    glUniform2fv(glGetUniformLocation(id, name.c_str()), 1, &value[0]);
}
void CreateShader::setVec2(const string &name, float x, float y) const{
    glUniform2f(glGetUniformLocation(id, name.c_str()), x, y);
}
// ------------------------------------------------------------------------
void CreateShader::setVec3(const string &name, const glm::vec3 &value) const{
    glUniform3fv(glGetUniformLocation(id, name.c_str()), 1, &value[0]);
}
void CreateShader::setVec3(const string &name, float x, float y, float z) const{
    glUniform3f(glGetUniformLocation(id, name.c_str()), x, y, z);
}
// ------------------------------------------------------------------------
void CreateShader::setVec4(const string &name, const glm::vec4 &value) const{
    glUniform4fv(glGetUniformLocation(id, name.c_str()), 1, &value[0]);
}
void CreateShader::setVec4(const string &name, float x, float y, float z, float w) const{
    glUniform4f(glGetUniformLocation(id, name.c_str()), x, y, z, w);
}
// ------------------------------------------------------------------------
void CreateShader::setMat2(const string &name, const glm::mat2 &mat) const{
    glUniformMatrix2fv(glGetUniformLocation(id, name.c_str()), 1, GL_FALSE, &mat[0][0]);
}
// ------------------------------------------------------------------------
void CreateShader::setMat3(const string &name, const glm::mat3 &mat) const{
    glUniformMatrix3fv(glGetUniformLocation(id, name.c_str()), 1, GL_FALSE, &mat[0][0]);
}
// ------------------------------------------------------------------------
void CreateShader::setMat4(const string &name, const glm::mat4 &mat) const{
    glUniformMatrix4fv(glGetUniformLocation(id, name.c_str()), 1, GL_FALSE, &mat[0][0]);
}
