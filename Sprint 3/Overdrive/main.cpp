#include <SFML/Graphics.hpp>
#define GLEW_STATC
#include <GL/glew.h>
#include <iostream>
#include <string>
#include <time.h>
/***Extern class***/
#include "CreateShader.h"
#include "Camera.h"
#include "Track.h"
#include "SkyBox.h"
#include "MakeQuad.h"
#include "Vehicle.h"
#include "Texte.h"
#include "FixedObjects.h"
#include "Hud.h"
/***OpenGL Maths***/
#include <glm/ext.hpp>


using namespace std;

int main(){

    /***Chronometer***/
    sf::Clock clock; //pour les fps
    sf::Clock timer; //current time
    sf::Time seconds = sf::seconds(10.0f); //loop every 10secs;
    std::string ms; //miliseconds

    /***Camera***/
    //           glm::vec3 camPos,              glm::vec3 camFront,           glm::vec3 camUp,             speed,pitch,yaw,intensity
    Camera myCam(glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(0.0f, 0.0f, -1.0f), glm::vec3(0.0f, 1.0f, 0.0f), 0.1, 0.0, 0.0, 1);

    /*Viewport*/
    sf::ContextSettings settings;
    settings.depthBits = 24;
    settings.stencilBits = 8;
    settings.antialiasingLevel = 3; // Optional
    // Request OpenGL version 3.2 (optional but recommended)
    settings.majorVersion = 3;
    settings.minorVersion = 3;
    settings.attributeFlags = sf::ContextSettings::Core;

    //1366x768
    sf::RenderWindow window(sf::VideoMode(1366, 768), "Overdrive", sf::Style::Close, settings);
    window.setFramerateLimit(60);
    /***Initializing openGL***/
    glewExperimental = GL_TRUE;
    glewInit();

    /*track*/
    Track muteCity(&myCam, window.getSize().x, window.getSize().y, "mute-city.png", "assets/mute-city_collision.png", glm::vec3(-8.0f, 0.0f, -14.0f), 6880, 3488, "Shaders/Vertex/vertexShader.vert", "Shaders/Fragment/fragmentShader.frag");
    /*Skybox*/
    SkyBox skyBox(&myCam);
    /*textes*/
    //Current time
    Texte lapSec(&myCam, window.getSize().x, window.getSize().y, "assets/numbers.png",198, 28, "Shaders/Vertex/texteVertex.vert", "Shaders/Fragment/texteFragment.frag");
    Texte lapSecDizaine(&myCam, window.getSize().x, window.getSize().y, "assets/numbers.png",198, 28, "Shaders/Vertex/texteVertex.vert", "Shaders/Fragment/texteFragment.frag");
    Texte lapMinute(&myCam, window.getSize().x, window.getSize().y, "assets/numbers.png",198, 28, "Shaders/Vertex/texteVertex.vert", "Shaders/Fragment/texteFragment.frag");
    Texte lapMinuteDizaine(&myCam, window.getSize().x, window.getSize().y, "assets/numbers.png",198, 28, "Shaders/Vertex/texteVertex.vert", "Shaders/Fragment/texteFragment.frag");
    Texte lapMilisec(&myCam, window.getSize().x, window.getSize().y, "assets/numbers.png",198, 28, "Shaders/Vertex/texteVertex.vert", "Shaders/Fragment/texteFragment.frag");
    Texte minSeparator(&myCam, window.getSize().x, window.getSize().y, "assets/numbers.png",198, 28, "Shaders/Vertex/texteVertex.vert", "Shaders/Fragment/texteFragment.frag");
    Texte secSeparator(&myCam, window.getSize().x, window.getSize().y, "assets/numbers.png",198, 28, "Shaders/Vertex/texteVertex.vert", "Shaders/Fragment/texteFragment.frag");
    //Record
    Texte recordLapMilisec(&myCam, window.getSize().x, window.getSize().y, "assets/numbers.png",198, 28, "Shaders/Vertex/texteVertex.vert", "Shaders/Fragment/texteFragment.frag");
    Texte recordLapSec(&myCam, window.getSize().x, window.getSize().y, "assets/numbers.png",198, 28, "Shaders/Vertex/texteVertex.vert", "Shaders/Fragment/texteFragment.frag");
    Texte recordLapSecDizaine(&myCam, window.getSize().x, window.getSize().y, "assets/numbers.png",198, 28, "Shaders/Vertex/texteVertex.vert", "Shaders/Fragment/texteFragment.frag");
    Texte recordLapMinute(&myCam, window.getSize().x, window.getSize().y, "assets/numbers.png",198, 28, "Shaders/Vertex/texteVertex.vert", "Shaders/Fragment/texteFragment.frag");
    Texte recordLapMinuteDizaine(&myCam, window.getSize().x, window.getSize().y, "assets/numbers.png",198, 28, "Shaders/Vertex/texteVertex.vert", "Shaders/Fragment/texteFragment.frag");
    Texte recordMinSeparator(&myCam, window.getSize().x, window.getSize().y, "assets/numbers.png",198, 28, "Shaders/Vertex/texteVertex.vert", "Shaders/Fragment/texteFragment.frag");
    Texte recordSecSeparator(&myCam, window.getSize().x, window.getSize().y, "assets/numbers.png",198, 28, "Shaders/Vertex/texteVertex.vert", "Shaders/Fragment/texteFragment.frag");
    /*HUD*/
    Hud hud(&lapMilisec, &lapSec, &lapSecDizaine, &lapMinute, &lapMinuteDizaine, &minSeparator, &secSeparator, &recordLapMilisec, &recordLapSec, &recordLapSecDizaine, &recordLapMinute, &recordLapMinuteDizaine, &recordMinSeparator, &recordSecSeparator);
    /*Vehicules*/
    Vehicle blueFalcon(&myCam, &muteCity, &hud, &timer, window.getSize().x, window.getSize().y, "assets/outrun.png",360, 30, "Shaders/Vertex/vehicleVertex.vert", "Shaders/Fragment/vehicleFragment.frag");
    /*Objets fixes (billboards)*/
    FixedObjects panneauTiger(&myCam, window.getSize().x, window.getSize().y, glm::vec3(-8.0f, 1.0f, -8.0f), "assets/billboard_tiger.png",500, 50, "Shaders/Vertex/billboardTiger.vert", "Shaders/Fragment/billboardTiger.frag");
    FixedObjects panneauCVM(&myCam, window.getSize().x, window.getSize().y, glm::vec3(8.0f, 1.0f, -8.0f), "assets/billboard_cvm.png",500, 50, "Shaders/Vertex/billboardTiger.vert", "Shaders/Fragment/billboardTiger.frag");


    /***DEPTH BUFFER***/
    // Enable depth test
    glEnable(GL_DEPTH_TEST);
    //Enable blending (pour la transparence)
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);
    // Accept fragment if it closer to the camera than the former one
    //glDepthFunc(GL_LESS);

    /***** BUFFERS *****/
    //Skybox
    int sb_arrayId = skyBox.loadData();
    int sb_texHandler = skyBox.getTextureData();

    /***Avec la class Shader***/
    CreateShader skyBoxShader("Shaders/Vertex/skyBoxShader.vert", "Shaders/Fragment/skyBoxShader.frag");

    /****************************************************/
    /***Test de création d'un track avec la class Quad***/
    /****************************************************/
    muteCity.createQuad();
    blueFalcon.createQuad();
    //laps.createQuad();
    panneauTiger.createQuad();
    panneauCVM.createQuad();
    /******************** MAIN LOOP ********************/
	// Start the game loop
	bool running = true;
    while (running){

        float currentTime = clock.restart().asSeconds();
        float fps = 1.0f / currentTime;
        //std::cout << "fps: " << fps << std::endl;

        /***Timer***/
        if(timer.getElapsedTime().asSeconds() >= seconds.asSeconds())
            timer.restart().asSeconds();//looper de 0 a 9 secondes
        ms = (std::to_string(int(timer.getElapsedTime().asMilliseconds())%1000))[0]; //prendre le 1er digit poiur les milisecondes
        hud.setChrono(glm::vec2(int(timer.getElapsedTime().asSeconds()), std::stoi(ms)));
        //std::cout << int(timer.getElapsedTime().asSeconds()) << " : " <<  std::stoi(milisecs[0]) << std::endl;
        //std::cout << std::stoi(ms) << std::endl;


        // Process events
        sf::Event event;
        while (window.pollEvent(event))
        {
            // Close window : exit
            if (event.type == sf::Event::Closed)
                running = false;
        }
        //window.setActive(true);

        /***Clear Buffers***/
        glClearColor(0.0f, 0.0f, 0.2f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        /***Draw Skybox***/
        glDepthMask(GL_FALSE);
        skyBoxShader.use();
        skyBox.draw(skyBoxShader.id);
        glBindVertexArray(sb_arrayId);
        glBindTexture(GL_TEXTURE_CUBE_MAP, sb_texHandler);
        glDrawArrays(GL_TRIANGLES, 0, 36);
        glDepthMask(GL_TRUE);

        /***Update***/
        myCam.moveCam();
        myCam.updateCam();
        blueFalcon.getTexCoord(muteCity);

        /***Run***/
        muteCity.run();
        hud.run();
        blueFalcon.run();
        panneauTiger.run();
        panneauCVM.run();

        window.display();
    }

    return 0;
}
