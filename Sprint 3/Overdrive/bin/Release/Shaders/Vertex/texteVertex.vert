#version 330 core

layout (location = 0) in vec3 aPos;   //get position
layout (location = 1) in vec3 aColor; //get color from vertice[]
layout (location = 2) in vec2 aTexCoord;//get tex coord from vertice[]

out vec3 myColor; //output color to fragment shader
out vec2 TexCoord; //output texture to fragment shader


//3D Stuff...
uniform mat4 model;
uniform mat4 vue;
uniform mat4 projection;

void main()
{
    gl_Position = projection * vue * model * vec4(aPos, 1.0f); //lire multiplication en arabe
    //gl_Position = projection * model * vec4(aPos.xy, 0.0f, 1.0f); //lire multiplication en arabe
    /*myColor = aColor;                                         //set color for fragment shader*/
    TexCoord = aTexCoord;
}
