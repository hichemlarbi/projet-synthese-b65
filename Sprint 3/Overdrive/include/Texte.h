#ifndef TEXTE_H
#define TEXTE_H

#include <SFML/Graphics.hpp>
#define GLEW_STATC
#include <GL/glew.h>
#include <iostream>
#include <math.h>
#include <string.h>
/***extern class***/
#include "Camera.h"
#include "MakeQuad.h"
#include "Track.h"
#include "Billboard.h"

/***OpenGL Maths***/
#include <glm/ext.hpp>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>


class Texte : public MakeQuad{
    public:
        Texte(Camera *cam, float screen_w, float screen_h, std::string texPath, int texWidth, int texHeight, const GLchar* vs, const GLchar* fs);

        /***Main events***/
        void event();
        void update();
        void draw();
        void run();

        /***Getters & Setters***/
        void setFrame();
        void setPos(glm::vec3 pos);
        CreateShader getShader();
        void setScale(float scale);
        glm::vec3 getOffset();
        void setOffset(glm::vec3 offset);
        glm::vec2 getCurPos();
        void setCurPos(glm::vec2 curPos);
        glm::vec2 getTexDimensions();



    private:
        /*Variables du vehicule*/
        glm::vec3 pos{0.0f, 0.0f, 0.0f};
        glm::vec3 target;
        glm::vec3 direction;
        glm::vec3 texteFront;
        glm::vec3 up;
        glm::vec3 right;

        /*Texture variables*/
        sf::Image *tex;
        int refreshRate = 0;
        int maxRefreshRate = 2;

        //nombre de lignes et de colonnes du spritesheet
        glm::vec2 texDimensions{1.0, 11.0};
        //texture offset for fragment shader
        glm::vec2 texOffset{1.0/texDimensions.y, 1.0};
        //current pos in the fragment shader
        glm::vec2 curPos{0.0f, 0.0f};
        //derniere colonne du spritesheet
        glm::vec2 texMax{texOffset.x * (texDimensions.y - 1), texOffset.y * (texDimensions.x - 1)};
        /*Variables de transformation du quad*/
        /***REVOIR LES OFFSETS POUR LE TEXTE***/
        float rotation{90.0f};
        float scale{0.015f};
        glm::vec3 offset{0.5f, 0.2f, -0.3f};

        /*Autres variables*/
        float curTime;
        Billboard outilBillboard;
};

#endif // TEXTE_H
