#ifndef VEHICLE_H
#define VEHICLE_H

#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#define GLEW_STATC
#include <GL/glew.h>
#include <iostream>
#include <math.h>
#include <string.h>
/***extern class***/
#include "Camera.h"
#include "MakeQuad.h"
#include "Track.h"
#include "Billboard.h"
#include "Hud.h"

/***OpenGL Maths***/
#include <glm/ext.hpp>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>


class Vehicle : public MakeQuad{
    public:
        Vehicle(Camera *cam, Track *track, Hud *hud, sf::Clock *chrono, float screen_w, float screen_h, std::string texPath, int texWidth, int texHeight, const GLchar* vs, const GLchar* fs);
        void transformQuad(); //Transformation model, vue, projection
        void getTexCoord(Track &terrain);

        void collide();
        void animate();
        void idleAnimation();

        glm::vec3 getPos();
        float getTurningRadius();

        //Sound
        void initSounds();
        void playEngineSounds();

        //collision
        void solveCollision();
        void marchingSquares();

        void event();
        void update();
        void draw();
        void run();

    protected:

    private:
        /***Variables du vehicule***/
        glm::vec3 vehiclePos;
        glm::vec3 target;
        glm::vec3 direction;
        glm::vec3 vehicleFront;
        glm::vec3 up;
        glm::vec3 right;
        //movement du vehicule
        glm::vec3 velocity{cam->getCameraPos()};
        float acceleration{0.0f};
        float friction{1.0f};
        float weight{0.75};
        float speed{0.001f};
        float topSpeed{0.1f};
        float initialTopSpeed{topSpeed};
        //turning radius
        float maxSpin{2.0f}; //hard drift
        float minSpin{0.75f};
        float turningRadius{minSpin}; //normal turning radius
        float driftForce{0.25f};
        float tireGrip{0.005f};
        float initialGrip{tireGrip};
        //drifting
        float maxDriftForce{0.075f};
        float gForce{0.005f}; //force lateral

        /***Collision***/
        glm::vec2 collisionPoint{0, 0};
        sf::Color stopAtGreen{0, 255, 0};
        glm::vec3 paralleleN{0, 0, 0};
        glm::vec3 vitesseParallele{0, 0, 0};
        glm::vec3 vitessePerpendiculaire{0, 0, 0};
        bool isCollidingExterior{false};
        bool isCollidingInterior{false};
        bool checkpointPassed{false};


        /***Texture variables***/
        sf::Image *tex;
        int refreshRate{0};
        int maxRefreshRate{2};

        //nombre de lignes et de colonnes du spritesheet
        glm::vec2 texDimensions{2.0, 6.0};
        //texture offset for fragment shader
        glm::vec2 texOffset{1.0/texDimensions.y, 1.0/texDimensions.x};
        //current pos in the fragment shader
        glm::vec2 curPos{0.0f, 0.0f};
        //derniere colonne du spritesheet
        glm::vec2 texMax{texOffset.x * (texDimensions.y - 1), texOffset.y * (texDimensions.x - 1)};
        //row of animation in the spritesheet
        float leftRow = 0.0f;
        float rightRow = 0.5f;
        /*Variables de transformation du quad*/
        float rotation{90.0f};
        float scale{0.05};
        glm::vec3 offset{0.5f, -0.1f, 0.0f};

        /***Audio***/
        //buffers
        sf::SoundBuffer upshiftBuffer;
        sf::SoundBuffer downshiftBuffer;
        sf::SoundBuffer idleBuffer;
        //sounds
        sf::Sound upshift;
        sf::Sound downshift;
        sf::Sound idle;
        //bool
        bool isUpshifting{false};
        bool isDownshifting{false};
        bool isIdling{false};

        /*Autres variables*/
        Billboard outilBillboard;
        Hud *hud;
        Track *track;
        std::string curCollision;
        sf::Clock *chrono;
        int gear{0};

};

#endif // VEHICLE_H






