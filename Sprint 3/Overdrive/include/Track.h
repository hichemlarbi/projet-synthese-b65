#ifndef TRACK_H
#define TRACK_H

#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#define GLEW_STATC
#include <GL/glew.h>
#include <iostream>
#include <math.h>
#include <string.h>
/***extern class***/
#include "Camera.h"
#include "MakeQuad.h"
/***OpenGL Maths***/
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

class Track : public MakeQuad
{
    public:
        Track(Camera *cam, float screen_w, float screen_h, std::string texPath, std::string collisionPath, glm::vec3 startLine, int texWidth, int texHeight, const GLchar* vs, const GLchar* fs);
        void transformQuad(); //Transformation model, vue, projection

        glm::vec2 getSize();
        glm::vec2 getTexSize();
        sf::Image &getCollisionTexture();
        std::string getObstacle(sf::Color color);
        void playMusic();
        //void event();
        void update();
        void draw();
        void run();

    private:
        /*Couleurs de collision*/
        sf::Color mud{sf::Color(255, 0, 255, 255)};
        sf::Color depart{sf::Color(0, 0, 255)};
        sf::Color wallExterior{sf::Color(0, 255, 0)};
        sf::Color wallInterior{sf::Color(255, 255, 0)};
        sf::Color checkpoint{sf::Color(150, 0, 255)};
        /*Variables de transformation du quad*/
        float rotation{90};
        float scale{20};
        std::string collisionSrc;
        sf::Image collisionTex;
        glm::vec3 startLine{0.0f, 0.0f, 0.0f};
        /*Audio*/
        //music
        sf::Music music;
        std::string playlist[6]{"assets/audio/music/Big_Blue.ogg",
                                "assets/audio/music/Mute_City.ogg",
                                "assets/audio/music/Port_Town.ogg",
                                "assets/audio/music/Red_Canyon.ogg",
                                "assets/audio/music/Top_Gear_1.ogg",
                                "assets/audio/music/White_Land_2.ogg"};


};

#endif // TRACK_H







