#include "Billboard.h"

Billboard::Billboard(){
    /**************************************************
        Outils de transformation de shader pour
        transformer une image en billboard
    ***************************************************/
}

void Billboard::makeBillboard(CreateShader &shader, Camera *cam, glm::vec3 position, float rotation, float scale){
    /**************************************************
        Modification du model, vue et projection pour
        avant l'envoi des données au shader
    ***************************************************/
    //Affichage selon la camera
    glm::mat4 model;

    /***Translate, rotate, scale (ordre important)***/
    //placer le vehicule devant la camera et "sur la piste"
    model = glm::translate(model,position);
    //rotation pour qu'il fasse face a la camera
    model = glm::rotate(model, glm::radians(rotation), glm::vec3(0.0f, 1.0f, 0.0f));
    //rotation pour qu'il fasse TOUJOURS face a la camera (rotation Yaw opposé a celle de la cam)
    model = glm::rotate(model, glm::radians(-cam->getYaw()), glm::vec3(0.0f, 1.0f, 0.0f));
    //rotation du sprite pour le mettre a l'endroit
    model = glm::rotate(model, glm::radians(180.0f), glm::vec3(0.0f, 0.0f, 1.0f));//mettre l'image a l'endroit
    //échelonnage du sprite pour avoir les bonnes dimensions
    model = glm::scale(model, glm::vec3(scale, scale, scale));  //scaling du vehicule

    /***envoi de la matrice de transformation au shader***/
    /*Model*/
    shader.setMat4("model", model);
    /*Vue*/
    shader.setMat4("vue", cam->getView());
    /*Projection*/
    shader.setMat4("projection", cam->getProjection());
}

void Billboard::orthoBillboard(CreateShader &shader, Camera *cam, glm::vec3 position, float rotation, float scale){

/**************************************************
        Modification du model, vue et projection pour
        avant l'envoi des données au shader
    ***************************************************/

    //                                left, right, bottom, top, near, far
    glm::mat4 projection = glm::ortho(0.0f, 800.0f, 600.0f, 00.0f, 0.001f, 1.0f);

    /***envoi de la matrice de transformation au shader***/

    /*Projection*/
    shader.setMat4("projection", projection);
}





