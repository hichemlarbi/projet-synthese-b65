#include "CubeMap.h"

CubeMap::CubeMap()
{
    //ctor
}

CubeMap::~CubeMap()
{
    //dtor
}

sf::Image CubeMap::loadImage(std::string path){
    if(!texture.loadFromFile(path)){
        std::cout << "Failed to load texture " << path << std::endl;
    }
    return texture;
}

int CubeMap::loadCubeMap(std::vector<std::string> faces){
    GLuint textureHandler;

    /***Loading textures***/
    //Texture object
    glGenTextures(1, &textureHandler);
    //bind a un cubemap
    glBindTexture(GL_TEXTURE_CUBE_MAP, textureHandler);

    //Chercher les 6 images pour chaque face du cube
    for(GLuint i = 0; i < faces.size(); i++){
        sf::Image curTexture = loadImage(faces[i]);
        //Upload image to GPU
        glTexImage2D(
            GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, GL_RGBA,
            texture.getSize().x, texture.getSize().y, 0,
            GL_RGBA, GL_UNSIGNED_BYTE, texture.getPixelsPtr()
        );

    }

    //image wraping if going out of bounds
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);


    return textureHandler;
}

