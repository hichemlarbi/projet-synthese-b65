#include "MakeQuad.h"

MakeQuad::MakeQuad(Camera *cam, float screen_w, float screen_h, std::string texPath, int texWidth, int texHeight, const GLchar* vs, const GLchar* fs){
    /**************************************************
        Constructeur pour la création d'un Quad:
        *Camera, window.w, window.h, texturePath,
        vertexShader.vs, fragmentShader.fs
    ***************************************************/

    this->cam = cam;
    this->screenSize = sf::Vector2f(screen_w, screen_h);
    this->shader = CreateShader(vs, fs);
    this->texPath = texPath;
    this->texWidth = texWidth;
    this->texHeight = texHeight;
}

sf::Image MakeQuad::loadImage(std::string src){
    /**************************************************
        Chargement d'une image dans l'objet sf::Image
    ***************************************************/
    sf::Image tex;
    if(!tex.loadFromFile(src)){
        std::cout << "Failed to load texture " << src << std::endl;
    }

    return tex;
}


void MakeQuad::initQuad(){
    /**************************************************
        Fonction pour la création d'un quad texturé
    ***************************************************/

    /***Coords***/
    float vertices[] = {
        //position       //texture
        -1.0, -1.0, 0.0, 0.0, 0.0,
         1.0, -1.0, 0.0, 1.0, 0.0,
         1.0,  1.0, 0.0, 1.0, 1.0,
         1.0,  1.0, 0.0, 1.0, 1.0,
        -1.0,  1.0, 0.0, 0.0, 1.0,
        -1.0, -1.0, 0.0, 0.0, 0.0,
    };

    /******************** BUFFERS ********************/
     /***Vertex Array***/
    unsigned int arrayId;
    glGenVertexArrays(1, &arrayId);
    glBindVertexArray(arrayId);

    /***Vertex Buffer***/
    unsigned int bufferId;
    glGenBuffers(1, &bufferId);
    glBindBuffer(GL_ARRAY_BUFFER, bufferId);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

    /***Interpretation***/
    /***location, size (vecX), type of elements, bool normalized, stride(space between sets), offset***/
    // position attribute
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)0);
    glEnableVertexAttribArray(0);
    // texture attribute
    glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)(3 * sizeof(float)));
    glEnableVertexAttribArray(2);

    //bindingIds
    this->arrayId = arrayId;

}

void MakeQuad::genTexture(){
    /**************************************************
        Fonction pour la création d'un quad texturé
    ***************************************************/

    /*** Chargement de l'image à appliquer sur le quad ***/
    GLuint textureHandler;

    /***Loading textures***/
    sf::Image image = loadImage(texPath);
    //Texture object
    glGenTextures(1, &textureHandler);
    glBindTexture(GL_TEXTURE_2D, textureHandler);

    //image wraping if going out of bounds
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    /***Mipmap***/
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST); //magnification does not use mipmapping

    //Upload image to GPU
    glTexImage2D(
        GL_TEXTURE_2D, 0, GL_RGBA,
        image.getSize().x, image.getSize().y, 0,
        GL_RGBA, GL_UNSIGNED_BYTE, image.getPixelsPtr()
    );

    this->textureHandler = textureHandler;
}

void MakeQuad::createQuad(){
    /**************************************************
        Méthode pour préparer les données nécessaires
        au rendering (avant la boucle principale)
    ***************************************************/

    initQuad();
    genTexture();
}



















