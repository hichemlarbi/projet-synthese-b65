#include "Texte.h"
Texte::Texte(Camera *cam, float screen_w, float screen_h, std::string texPath, int texWidth, int texHeight, const GLchar* vs, const GLchar* fs)
        :MakeQuad(cam, screen_w, screen_h, texPath, texWidth, texHeight, vs, fs) {
    //ctor
}
void Texte::event(){}

void Texte::update(){
    pos = cam->getCameraPos() + (cam->getCameraFront() * offset.x) + (cam->getCameraUp() * offset.y) + (cam->getCameraRight() * offset.z);
}

void Texte::draw(){
    /**************************************************
        Méthode pour dessiner la forme selon les
        données voulu
    ***************************************************/
    //Draw
    this->shader.use(); //use the shader

    /***Chercher la bonne texture et le binder***/
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, textureHandler);
    this->shader.setInt("ourTexture", 0);
    //Crop la spritesheet
    this->shader.setVec2("pos", curPos);
    this->shader.setVec2("size", texOffset);
    /***Chercher l'array et le binder***/
    glBindVertexArray(arrayId); //bind array
    //Envoi du billboarding au shader
    outilBillboard.makeBillboard(this->shader, this->cam, pos, rotation, scale);
    //outilBillboard.orthoBillboard(this->shader, this->cam, pos, rotation, scale);
    /***Dessiner***/
    glDrawArrays(GL_TRIANGLES, 0, 36); //draw all binded arrays of triangles
    /***Unbind l'array***/
    glBindVertexArray(0);
}
void Texte::run(){
    //std::cout << "lap : " << glm::to_string(pos) << std::endl;
    this->event();
    this->update();
    this->draw();
}

/***Getters & Setters***/
void Texte::setPos(glm::vec3 pos){
    this->pos = pos;
}
void Texte::setScale(float scale){
    this->scale = scale;
}
glm::vec3 Texte::getOffset(){
    return offset;
}
void Texte::setOffset(glm::vec3 offset){
    this->offset = offset;
}
glm::vec2 Texte::getCurPos(){
    return curPos;
}

void Texte::setCurPos(glm::vec2 curPos){
    this->curPos = curPos;
}

glm::vec2 Texte::getTexDimensions(){
    return texDimensions;
}




