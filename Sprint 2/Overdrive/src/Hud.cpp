#include "Hud.h"

Hud::Hud(Texte *lapMilisec, Texte *lapSec, Texte *lapSecDizaine, Texte *lapMinute, Texte *lapMinuteDizaine, Texte *minSeparator, Texte *secSeparator,
        Texte *recordLapMilisec, Texte *recordLapSec, Texte *recordLapSecDizaine, Texte *recordLapMinute, Texte *recordLapMinuteDizaine, Texte *recordMinSeparator, Texte *recordSecSeparator){

    /***CURRENT LAP***/
    //Sprites pour le compteur de laps
    this->lapMilisec = lapMilisec;
    this->secSeparator = secSeparator;
    this->lapSec = lapSec;
    this->lapSecDizaine = lapSecDizaine;
    this->minSeparator = minSeparator;
    this->lapMinute = lapMinute;
    this->lapMinuteDizaine = lapMinuteDizaine;
    /***RECORD***/
    this->recordLapMilisec = recordLapMilisec;
    this->recordSecSeparator = recordSecSeparator;
    this->recordLapSec = recordLapSec;
    this->recordLapSecDizaine = recordLapSecDizaine;
    this->recordMinSeparator = recordMinSeparator;
    this->recordLapMinute = recordLapMinute;
    this->recordLapMinuteDizaine = recordLapMinuteDizaine;


    //create the sprites for the timer
    /***Current lap***/
    this->lapMilisec->createQuad();
    this->secSeparator->createQuad();
    this->lapSec->createQuad();
    this->lapSecDizaine->createQuad();
    this->minSeparator->createQuad();
    this->lapMinute->createQuad();
    this->lapMinuteDizaine->createQuad();
    /***Record***/
    this->recordLapMilisec->createQuad();
    this->recordSecSeparator->createQuad();
    this->recordLapSec->createQuad();
    this->recordLapSecDizaine->createQuad();
    this->recordMinSeparator->createQuad();
    this->recordLapMinute->createQuad();
    this->recordLapMinuteDizaine->createQuad();

    //scaling down record time
    this->recordLapMilisec->setScale(0.01f);
    this->recordSecSeparator->setScale(0.01f);
    this->recordLapSec->setScale(0.01f);
    this->recordLapSecDizaine->setScale(0.01f);
    this->recordMinSeparator->setScale(0.01f);
    this->recordLapMinute->setScale(0.01f);
    this->recordLapMinuteDizaine->setScale(0.01f);


    //décalage vers la gauche des sprites
    recordLapMilisec->setOffset(glm::vec3(lapMilisec->getOffset().x, lapMilisec->getOffset().y - 0.05, lapMilisec->getOffset().z));
    this->secSeparator->setOffset(glm::vec3(lapMilisec->getOffset().x, lapMilisec->getOffset().y, lapMilisec->getOffset().z + 0.03));
    recordSecSeparator->setOffset(glm::vec3(lapMilisec->getOffset().x, lapMilisec->getOffset().y - 0.05, lapMilisec->getOffset().z + 0.03));
    this->lapSec->setOffset(glm::vec3(secSeparator->getOffset().x, secSeparator->getOffset().y, secSeparator->getOffset().z + 0.03));
    recordLapSec->setOffset(glm::vec3(secSeparator->getOffset().x, secSeparator->getOffset().y - 0.05, secSeparator->getOffset().z + 0.03));
    this->lapSecDizaine->setOffset(glm::vec3(lapSec->getOffset().x, lapSec->getOffset().y, lapSec->getOffset().z + 0.03));
    recordLapSecDizaine->setOffset(glm::vec3(lapSec->getOffset().x, lapSec->getOffset().y - 0.05, lapSec->getOffset().z + 0.03));
    this->minSeparator->setOffset(glm::vec3(lapSecDizaine->getOffset().x, lapSecDizaine->getOffset().y, lapSecDizaine->getOffset().z + 0.03));
    recordMinSeparator->setOffset(glm::vec3(lapSecDizaine->getOffset().x, lapSecDizaine->getOffset().y - 0.05, lapSecDizaine->getOffset().z + 0.03));
    this->lapMinute->setOffset(glm::vec3(minSeparator->getOffset().x, minSeparator->getOffset().y, minSeparator->getOffset().z + 0.03));
    recordLapMinute->setOffset(glm::vec3(minSeparator->getOffset().x, minSeparator->getOffset().y - 0.05, minSeparator->getOffset().z + 0.03));
    this->lapMinuteDizaine->setOffset(glm::vec3(lapMinute->getOffset().x, lapMinute->getOffset().y, lapMinute->getOffset().z + 0.03));
    recordLapMinuteDizaine->setOffset(glm::vec3(lapMinute->getOffset().x, lapMinute->getOffset().y - 0.05, lapMinute->getOffset().z + 0.03));
}

void Hud::event(){
    /***mettre a jour le chrono***/
    curLapTime[0] = chrono.y;
    curLapTime[1] = chrono.x;
    //secondes
    if(chrono.x == 0 && chrono.y == 0){
        secondeDizaine += 1;
        curLapTime[2] = secondeDizaine;
    }

    //a 60 sec, on a 1 minute
    if(secondeDizaine == 6 && chrono.x == 0 && chrono.y == 0){
        secondeDizaine = 0;
        minute += 1;
        curLapTime[3] = minute;
    }

    //apres 9 minutes, on a +1 dizaine de minute
    if(minute > 9 && secondeDizaine == 0 && chrono.x == 0 && chrono.y == 0){
        minute = 0;
        minuteDizaine += 1;
        curLapTime[4] = minuteDizaine;
    }

}

void Hud::update(){
    //definir la valeur de milisecondes
    lapMilisec->setCurPos(glm::vec2((chrono.y/lapMilisec->getTexDimensions().y), 0.0f));
    recordLapMilisec->setCurPos(glm::vec2((bestLapTime[0]/recordLapMilisec->getTexDimensions().y), 0.0f));
    //definir le separateur de secondes
    secSeparator->setCurPos(glm::vec2((10/secSeparator->getTexDimensions().y), 0.0f));
    recordSecSeparator->setCurPos(glm::vec2((10/recordSecSeparator->getTexDimensions().y), 0.0f));
    //definir les secondes
    lapSec->setCurPos(glm::vec2((chrono.x/lapSec->getTexDimensions().y), 0.0f));
    lapSecDizaine->setCurPos(glm::vec2((secondeDizaine/lapSecDizaine->getTexDimensions().y), 0.0f));
    //definir le separateur de minutes
    minSeparator->setCurPos(glm::vec2((10/secSeparator->getTexDimensions().y), 0.0f));
    recordMinSeparator->setCurPos(glm::vec2((10/recordMinSeparator->getTexDimensions().y), 0.0f));
    //definir les minutes
    lapMinute->setCurPos(glm::vec2((minute/lapMinute->getTexDimensions().y), 0.0f));
    lapMinuteDizaine->setCurPos(glm::vec2((minuteDizaine/lapMinuteDizaine->getTexDimensions().y), 0.0f));

    /***définir les valeurs du record courant***/
    recordLapMilisec->setCurPos(glm::vec2((bestLapTime[0]/recordLapMilisec->getTexDimensions().y), 0.0f));
    //definir le separateur de secondes
    recordSecSeparator->setCurPos(glm::vec2((10/recordSecSeparator->getTexDimensions().y), 0.0f));
    //definir les secondes
    recordLapSec->setCurPos(glm::vec2((bestLapTime[1]/lapSec->getTexDimensions().y), 0.0f));
    recordLapSecDizaine->setCurPos(glm::vec2((bestLapTime[2]/lapSecDizaine->getTexDimensions().y), 0.0f));
    //definir le separateur de minutes
    recordMinSeparator->setCurPos(glm::vec2((10/recordMinSeparator->getTexDimensions().y), 0.0f));
    //definir les minutes
    recordLapMinute->setCurPos(glm::vec2((bestLapTime[3]/lapMinute->getTexDimensions().y), 0.0f));
    recordLapMinuteDizaine->setCurPos(glm::vec2((bestLapTime[4]/lapMinuteDizaine->getTexDimensions().y), 0.0f));


}

void Hud::draw(){}

void Hud::run(){
    event();
    update();
    draw();

    /***Current Lap***/
    lapMilisec->run();
    secSeparator->run();
    lapSec->run();
    lapSecDizaine->run();
    minSeparator->run();
    lapMinute->run();
    lapMinuteDizaine->run();

    /***Record***/
    this->recordLapMilisec->run();
    this->recordSecSeparator->run();
    this->recordLapSec->run();
    this->recordLapSecDizaine->run();
    this->recordMinSeparator->run();
    this->recordLapMinute->run();
    this->recordLapMinuteDizaine->run();
}

void Hud::resetChrono(){
    secondeDizaine = 0;
    minute = 0;
    minuteDizaine = 0;
}

/***Getters & Setters***/
void Hud::setChrono(glm::vec2 chrono){
    this->chrono = chrono;
}
int* Hud::getLapTime(){
    //std::stoi(milisecs[0])
    std::string curMinutes = std::to_string(curLapTime[4]) + std::to_string(curLapTime[3]);
    std::string curSeconds = std::to_string(curLapTime[2]) + std::to_string(curLapTime[1]);
    std::string bestMinutes = std::to_string(bestLapTime[4]) + std::to_string(bestLapTime[3]);
    std::string bestSeconds = std::to_string(bestLapTime[2]) + std::to_string(bestLapTime[1]);

    std::cout << "curLap  : " << curMinutes << ":" << curSeconds << ":" << curLapTime[0] << std::endl;
    //std::cout << "curLap  : " << curLapTime[4] << curLapTime[3] << ":" << curLapTime[2] << curLapTime[1] << ":" << curLapTime[0] << std::endl;
    //std::cout << "bestLap : " << bestLapTime[4] << bestLapTime[3] << ":" << bestLapTime[2] << bestLapTime[1] << ":" << bestLapTime[0] << std::endl;

    //comparaison du temps
    if(std::stoi(curMinutes) < std::stoi(bestMinutes))
        setBestLapTime();

    else if(std::stoi(curSeconds) < std::stoi(bestSeconds))
        setBestLapTime();

    else if(curLapTime[0] < bestLapTime[0])
        setBestLapTime();

}

void Hud::setBestLapTime(){
    bestLapTime[0] = curLapTime[0];
    bestLapTime[1] = curLapTime[1];
    bestLapTime[2] = curLapTime[2];
    bestLapTime[3] = curLapTime[3];
    bestLapTime[4] = curLapTime[4];
}




