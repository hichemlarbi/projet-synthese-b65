#include "Track.h"

using namespace std;

Track::Track(Camera *cam, float screen_w, float screen_h, std::string texPath, std::string collisionPath, glm::vec3 startLine, int texWidth, int texHeight, const GLchar* vs, const GLchar* fs)
        : MakeQuad(cam, screen_w, screen_h, texPath, texWidth, texHeight, vs, fs) {
        collisionTex = loadImage(collisionPath);
        this->startLine = startLine;
        //Placer la camera a la ligne de depart
        cam->setCameraPos(this->startLine);

        //initialiser la musique



}

void Track::transformQuad(){
    /**************************************************
        Modification du model, vue et projection pour
        avant l'envoi des données au shader
    ***************************************************/

    //Affichage selon la camera
    glm::mat4 model;


    //rotating -90 rad on X-axis
    model = glm::rotate(model, glm::radians(90.0f), glm::vec3(1.0f, 0.0f, 0.0f));
    //model = glm::rotate(model, glm::radians(-15.0f), glm::vec3(0.0f, 1.0f, 0.0f));
    //translate la map
    model = glm::translate(model,glm::vec3(0.0f,  0.0f, 0.3f));
    model = glm::scale(model, glm::vec3(scale, scale, scale));
    //translation de l'écran vers l'avant (right-handed system)
    cam->setView(glm::translate(cam->getView(), glm::vec3(0.0f, 0.0f, -1.0f)));
    cam->setProjection(glm::perspective(glm::radians(53.0f), screenSize.x/screenSize.y, 0.1f, 100.0f));

    //pour usage externe du model
    cam->setModel(model);

    /***Camera***/
    cam->setView(cam->getViewMat());

    /***envoi de la matrice de transformation au shader***/
    /*Model*/
    this->shader.setMat4("model", model);
    /*Vue*/
    this->shader.setMat4("vue", cam->getView());
    /*Projection*/
    this->shader.setMat4("projection", cam->getProjection());
    //std::cout << "camera front : " << cam->getCameraFront().x << "," << cam->getCameraFront().y << ", " << cam->getCameraFront().z << endl;
}

void Track::playMusic(){
    int trackNumber = rand() % 6;

    std::cout << "music initialised at " << trackNumber;

    if (!music.openFromFile(playlist[trackNumber]))
        std::cout << "failed to load " << playlist[trackNumber] << std::endl;
    music.play();
}

glm::vec2 Track::getSize(){
    return glm::vec2(1.0f, 1.0f) * scale;
}

glm::vec2 Track::getTexSize(){
    return glm::vec2(texWidth, texHeight);
}

sf::Image& Track::getCollisionTexture(){
    return collisionTex;
}

void Track::update(){
    if(music.getStatus() == 0){
        playMusic();
    }
}

void Track::draw(){
    /**************************************************
        Méthode pour dessiner la forme selon les
        données voulu
    ***************************************************/

    this->shader.use(); //use the shader
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, textureHandler);
    this->shader.setInt("ourTexture", 0);

    glBindVertexArray(arrayId); //bind array
    transformQuad(); //transform quad
    glDrawArrays(GL_TRIANGLES, 0, 36); //draw all binded arrays of triangles
    glBindVertexArray(0);

}

void Track::run(){
    update();
    draw();
}

std::string Track::getObstacle(sf::Color color){
    if(color == mud)
        return "mud";
    if(color == depart)
        return "depart";
    if(color == wallExterior)
        return "wallExterior";
    if(color == wallInterior)
        return "wallInterior";
    if(color == checkpoint)
        return "checkpoint";

    return "road";
}








