#include "Vehicle.h"

Vehicle::Vehicle(Camera *cam, Track *track, Hud *hud, sf::Clock *chrono, float screen_w, float screen_h, std::string texPath, int texWidth, int texHeight, const GLchar* vs, const GLchar* fs)
        :MakeQuad(cam, screen_w, screen_h, texPath, texWidth, texHeight, vs, fs) {
    //ctor
    this->hud = hud;
    this->chrono = chrono;
    this->track = track;
    tex = &track->getCollisionTexture();
    initSounds();
}

void Vehicle::initSounds(){
    /***Audio***/
    //upshift
    if (!upshiftBuffer.loadFromFile("assets/audio/upshift.ogg"))
        std::cout << "failed to buffer upshift.ogg" << std::endl;
    upshift.setBuffer(upshiftBuffer);

    //downshift
    if (!downshiftBuffer.loadFromFile("assets/audio/downshift.ogg"))
        std::cout << "failed to buffer downshift.ogg" << std::endl;
    downshift.setBuffer(downshiftBuffer);

    //downshift
    if (!idleBuffer.loadFromFile("assets/audio/idle.ogg"))
        std::cout << "failed to buffer idle.ogg" << std::endl;
    idle.setBuffer(idleBuffer);
    idle.setVolume(50.0f);

}

void Vehicle::getTexCoord(Track &terrain){
    //Position du vehicule
    //glm::vec2 p(0, 0);
    sf::Color collisionColor;
    collisionPoint.x = vehiclePos.x + cam->getCameraFront().x;
    collisionPoint.y = vehiclePos.z + cam->getCameraFront().z;
    //position selon la taille du terrain
    collisionPoint += track->getSize();
    //"normalisation" des coordonnees
    collisionPoint.x /= track->getSize().x * 2;
    collisionPoint.y /= track->getSize().y * 2;
    //position en pixel sur la texture
    collisionPoint.x *= track->getTexSize().x;
    collisionPoint.y *= track->getTexSize().y;

    //Si je suis sur la texture, voir la couleur du pixel en dessous de moi
    if((collisionPoint.x >= 0 && collisionPoint.x < track->getTexSize().x) &&
        (collisionPoint.y >= 0 && collisionPoint.y < track->getTexSize().y)){
        //solve collision
        collisionColor = tex->getPixel(collisionPoint.x, collisionPoint.y);
        //std::cout << int(collisionColor.r) << ", " << int(collisionColor.g) << ", " << int(collisionColor.b) << std::endl;
        curCollision = track->getObstacle(collisionColor);
    }
    //std::cout << "curCollision : " << curCollision << std::endl;
    //std::cout << p.x << ", " << p.y << std::endl;
}

void Vehicle::marchingSquares(){
    //std::cout collisionPoint.x << ", " << collisionPoint.y << std::endl;
    glm::vec2 pointsX[6]; //calcul des points verticalement
    glm::vec2 pointsY[6]; //calcul des points horizontalement
    glm::vec2 finalX(0, 0);
    glm::vec2 finalY(0, 0);
    glm::vec2 finalLine(0, 0);
    int c = 0;
    int arraySize = 0;

    sf::Color oldSurfaceColor; //couleur precedente
    sf::Color surfaceColor; //couleur courrante
    std::string oldSurface; //surface precedente
    std::string surface; //surface courante

    //Si je suis sur la texture, voir la couleur du pixel en dessous de moi
    if((collisionPoint.x >= 0 && collisionPoint.x < track->getTexSize().x) &&
        (collisionPoint.y >= 0 && collisionPoint.y < track->getTexSize().y)){

        //vérification de colonnes (verticale)
        //std::cout << "VERTICAL\n" << std::endl;
        for(int lig = collisionPoint.y - 1; lig <= collisionPoint.y + 1; lig++){
            for(int col = collisionPoint.x - 1; col <= collisionPoint.x + 1; col++){
                //Définir la couleur courante
                surfaceColor = tex->getPixel(col, lig);
                surface = track->getObstacle(surfaceColor);
                //std::cout << "START POINT = " << collisionPoint.x << ", " << collisionPoint.y << std::endl;
                //std::cout << "(col: " << col << ", lig: " << lig << ") = " << surface << std::endl;
                //Définir l'ancienne couleur
                if(col != collisionPoint.x - 1 ){
                    //voir la couleur precedente
                    oldSurfaceColor = tex->getPixel(col - 1, lig);
                    oldSurface = track->getObstacle(oldSurfaceColor);
                }
                else{
                    //couleur precedente = couleur courante
                    oldSurfaceColor = surfaceColor;
                    oldSurface = surface;
                }
                //Si le point est different, le sauvegarder (OU sauvegarder l'ancien?)
                if(surface != oldSurface){
                    pointsX[c] = glm::vec2(col, lig);
                    c++;
                }
            }
        }

        //reset compteur
        c = 0;
        //std::cout << "old surface : " << oldSurface << std::endl;
        //std::cout << "new surface : " << surface << std::endl;


        //afficher les points
        for(int i = 0; i < 6; i++){
            //std::cout << std::endl << "pointX " << i + 1 << " [" << pointsX[i].x << ", " << pointsX[i].y << "]" << std::endl;
            finalX += pointsX[i];
            //compter le nombre d'elements pour faire une moyenne apres
            if(pointsX[i].x != 0 && pointsX[i].y != 0)
                arraySize += 1; //probleme avec le size de mon array...
        }
        if(arraySize != 0)
            finalX /= arraySize;
        arraySize = 0; //reset pour usage ulterieurement
        //std::cout << std::endl << "FINALX " << " [" << finalX.x << ", " << finalX.y << "]" << std::endl;
        //std::cout << "---------------------------------------------------------\n\n" << std::endl;

        //vérification de lignes (Horizontales)
        //std::cout << "HORIZONTAL\n" << std::endl;
        for(int col = collisionPoint.x - 1; col <= collisionPoint.x + 1; col++){
            for(int lig = collisionPoint.y - 1; lig <= collisionPoint.y + 1; lig++){
                                //Définir la couleur courante
                surfaceColor = tex->getPixel(col, lig);
                surface = track->getObstacle(surfaceColor);
                //std::cout << "START POINT = " << collisionPoint.x << ", " << collisionPoint.y << std::endl;
                //std::cout << "(col: " << col << ", lig: " << lig << ") = " << surface << std::endl;
                //Définir l'ancienne couleur
                if(lig != collisionPoint.y - 1){
                    oldSurfaceColor = tex->getPixel(col, lig - 1);
                    oldSurface = track->getObstacle(oldSurfaceColor);
                }
                else{
                    oldSurfaceColor = surfaceColor;
                    oldSurface = surface;
                }
                //Si le point est different, le sauvegarder (OU sauvegarder l'ancien?)
                if(surface != oldSurface){
                    pointsY[c] = glm::vec2(col, lig);
                    c++;
                }
            }
        }

        //reset compteur
        c = 0;
        //afficher les points
        for(int i = 0; i < 6; i++){
            //std::cout << std::endl << "pointsY " << i + 1 << " [" << pointsY[i].x << ", " << pointsY[i].y << "]" << std::endl;
            finalY += pointsY[i];
            //compter le nombre d'elements pour faire une moyenne apres
            if(pointsY[i].x != 0 && pointsY[i].y != 0)
                arraySize += 1; //probleme avec le size de mon array...
        }
        if(arraySize != 0)
            finalY /= arraySize;
        //std::cout << std::endl << "FINALY " << " [" << finalY.x << ", " << finalY.y << "]" << std::endl;
        if((finalX.x != 0 && finalX.y  != 0) || (finalY.x != 0 && finalY.y  != 0))
            finalLine = glm::normalize(finalX - finalY);
        //std::cout << "FINAL LINE after  : " << glm::to_string(finalLine) << std::endl;
        //std::cout << "normalized vector : " << glm::to_string(glm::normalize(finalLine)) << std::endl;
        //std::cout << "---------------------------------------------------------\n\n" << std::endl;

    }

    //Calcul du vecteur parallele a la surface normalise
    paralleleN = glm::vec3(finalLine.y, 0, -finalLine.x);
    if((paralleleN.x != 0 && paralleleN.z  != 0))
        vitesseParallele = (paralleleN.x * velocity.x + paralleleN.y * velocity.y) * paralleleN;
        //vitesseParallele = glm::dot(paralleleN, velocity) * paralleleN;
    vitessePerpendiculaire = velocity - vitesseParallele;

    /*std::cout << "velocity               : " << glm::to_string(velocity) << std::endl;
    std::cout << "ParalleleN             : " << glm::to_string(paralleleN) << std::endl;
    std::cout << "vitesseParallele       : " << glm::to_string(vitesseParallele) << std::endl;
    std::cout << "vitessePerpendiculaire : " << glm::to_string(vitessePerpendiculaire) << std::endl;
    std::cout << "-------------------------------------------------------------------" << std::endl;*/

}

void Vehicle::animate(){
    //Compteur pour une animation plus fluide
    if(refreshRate <= maxRefreshRate)
        refreshRate += 1;
    else
        refreshRate = 0;

    //Animer le virage gauche et droite
    if(refreshRate == maxRefreshRate){
        //animate turn right
        if(cam->getTurningRight()){
            curPos.y = rightRow;
            if(curPos.x != texMax.x){
                curPos.x += texOffset.x;
            }

        }
        //animate left
        else if(cam->getTurningLeft()){
            curPos.y = leftRow;
            if(curPos.x != texMax.x){
                curPos.x += texOffset.x;
            }

        }
        else{
            //revenir a la position initial si on ne tourne pas
            if(curPos.x > 0.0f){
                curPos.x -= texOffset.x;
            }
        }

    }
}

void Vehicle::event(){
    //Appliquer la frictoin
    speed *= friction;
    //drift & turn
    if((cam->getTurningLeft() && cam->getDrifting()) || (cam->getTurningRight() && cam->getDrifting())){
        if(turningRadius < maxSpin){
            turningRadius += driftForce;
        }
        //increment drift gradually
        if(tireGrip <= maxDriftForce)
            tireGrip += gForce;
    }
    else{
        if(turningRadius > minSpin){
            turningRadius -= driftForce;
        }
        if(tireGrip > initialGrip){
            tireGrip -= gForce;
        }
    }

    cam->setIntensity(turningRadius);

    //Accelerer
    if(cam->getAccelerating()){
        //vitesse proceduralement croissante
        if(acceleration < topSpeed){
            acceleration += speed;
        }
        //sound upshift
        isUpshifting = true;
        isDownshifting = false;
        isIdling = false;

    }
    else{
        isUpshifting = false;
        isDownshifting = true;
        isIdling = false;
    }



    //reverse
    if(cam->getReversing()){
        //std::cout << "acceleration : " << acceleration << std::endl;
        if(acceleration > -topSpeed/2){
            acceleration -= speed;
        }

    }

    //vitesse proceduralement decroissante
    if(acceleration > 0.00075){
        acceleration -= speed * weight;
        //sound downshift
    }
    else if(acceleration <= -0.00075){
        acceleration += speed * weight;
    }
    else{
        acceleration = 0.0f;
        isUpshifting = false;
        isDownshifting = false;
        isIdling = true;
    }
}

void Vehicle::update(){
    //std::cout << "gear " << gear << std::endl;
    /***Get collision***/
    getTexCoord(*track);

    /***Adjust speed***/
    if(acceleration > topSpeed)
        acceleration -= speed;

    /***Solve collision***/
    solveCollision();
    //Marching squares

    /***lookAt Camera***/
    //Positionner le vehicule devant la camera
    vehiclePos = cam->getCameraPos() + (cam->getCameraFront() * offset.x) + (cam->getCameraUp() * offset.y) + (cam->getCameraRight() * offset.z);
    //Appliquer la velocite du vehicule
    if(isCollidingExterior){
        std::cout << "exterior colliding" << std::endl;
        velocity = cam->getCameraPos() + ((acceleration * (cam->getCameraFront() * glm::normalize(vitesseParallele + vitessePerpendiculaire))));
        velocity += glm::normalize(-vitessePerpendiculaire) * 0.1;
    }
    else if(isCollidingInterior){
        std::cout << "interior colliding" << std::endl;
        velocity = cam->getCameraPos() + ((acceleration * (cam->getCameraFront() * glm::normalize(vitesseParallele + vitessePerpendiculaire))));
        velocity += glm::normalize(+vitessePerpendiculaire) * 0.1;
    }
    else
        velocity = cam->getCameraPos() + (acceleration * cam->getCameraFront());
    //velocity = cam->getCameraPos() + (acceleration * (cam->getCameraFront() * glm::normalize(vitesseParallele + vitessePerpendiculaire)));
    //drift
    if(cam->getTurningLeft() && cam->getDrifting()){
        velocity += glm::normalize(-cam->getCameraRight()) * tireGrip;
    }
    if(cam->getTurningRight() && cam->getDrifting()){
        velocity += glm::normalize(cam->getCameraRight()) * tireGrip;
    }
    cam->setCameraPos(velocity);

    /***Update sounds***/
    playEngineSounds();

    marchingSquares();
    //std::cout << "velocity : " << glm::to_string(velocity) << std::endl;
}

void Vehicle::draw(){
    /**************************************************
        Méthode pour dessiner la forme selon les
        données voulu
    ***************************************************/
    //Draw
    this->shader.use(); //use the shader

    /***Animation***/
    animate();
    /***Chercher la bonne texture et le binder***/
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, textureHandler);
    this->shader.setInt("ourTexture", 0);
    //Crop la spritesheet
    this->shader.setVec2("pos", curPos);
    this->shader.setVec2("size", texOffset);
    /***Chercher l'array et le binder***/
    glBindVertexArray(arrayId); //bind array
    //Envoi du billboarding au shader
    outilBillboard.makeBillboard(this->shader, this->cam, vehiclePos, rotation, scale);

    /***Dessiner***/
    glDrawArrays(GL_TRIANGLES, 0, 36); //draw all binded arrays of triangles
    /***Unbind l'array***/
    glBindVertexArray(0);
}

void Vehicle::run(){
    event();
    update();
    draw();
}

void Vehicle::solveCollision(){
    if(curCollision == "mud"){
        upshift.setPitch(0.9f);
        topSpeed = 0.03f;
    }

    else if(curCollision == "depart"){
        if(checkpointPassed){
            hud->getLapTime();
            chrono->restart().asSeconds();
            hud->resetChrono();
            checkpointPassed = false;
        }

    }
    else if(curCollision == "wallExterior"){
        isCollidingExterior = true;
    }
    else if(curCollision == "wallInterior"){
        isCollidingInterior = true;
    }
    else if(curCollision == "checkpoint"){
        std::cout << "CHECKPOINT" << std::endl;
        checkpointPassed = true;
    }
    else{
        upshift.setPitch(1.0f);
        topSpeed = initialTopSpeed;
        isCollidingExterior = false;
        isCollidingInterior = false;
    }


}
/***Audio***/
void Vehicle::playEngineSounds(){

    //std::cout << "status " << upshift.getStatus() << std::endl;

    //if accelerating
    if(isUpshifting){
        //if not already playing, play it
        if(upshift.getStatus() == 0 || upshift.getStatus() == 1){
            upshift.play();
        }
    }
    else{
        //upshift.stop();
        upshift.pause();
    }

    //if decelerating
    if(isDownshifting){
        //if not already playing, play it
        if(downshift.getStatus() == 0 || downshift.getStatus() == 1){
            downshift.play();
        }
    }
    else{
        downshift.stop();
        //downshift.pause();
    }

    //if idling
    if(isIdling){
        //if not already playing, play it
        if(idle.getStatus() == 0 || idle.getStatus() == 1){
            idle.play();
            upshift.stop(); //reset upshift when completely stopped
        }
    }
    else{
        idle.stop();
        //idle.pause();
    }
}

/***Getters & Setters***/
glm::vec3 Vehicle::getPos(){
    return vehiclePos;
}
float Vehicle::getTurningRadius(){
    return turningRadius;
}


