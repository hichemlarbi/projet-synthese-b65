#ifndef MAKEQUAD_H
#define MAKEQUAD_H

#include <SFML/Graphics.hpp>
#define GLEW_STATC
#include <GL/glew.h>
#include <iostream>
#include <math.h>
#include <string.h>
/***extern class***/
#include "Camera.h"
#include "CreateShader.h"
/***OpenGL Maths***/
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

/**************************************************
    Class MakeShape :
        Classe servant à la création de formes
        OpenGL, notamment pour créer des planes
        et des cubes
***************************************************/

class MakeQuad
{
    public:
        MakeQuad(Camera *cam, float screen_w, float screen_h, std::string texPath, int texWidth, int texHeight, const GLchar* vs, const GLchar* fs);
        void initQuad(); //Envoi de données au GPU (VBO, VAO...)
        void genTexture(); //Chargement de la texture
        void createQuad(); //appel des fonctions pour initialiser les données nécessaire
        sf::Image loadImage(std::string src); //charger l'image

    protected:
        /***Variables***/
        Camera *cam; //camera
        sf::Vector2f screenSize; //screen coord
        sf::Image image;//image (texture) du quad
        CreateShader shader; //shader
        std::string texPath;
        int texWidth, texHeight;
        sf::IntRect texRect = sf::IntRect(0, 0, this->texWidth, this->texHeight);
        int arrayId; //array pour le binding
        int textureHandler; //texture pour le binding


    private:




};

#endif // MAKEQUAD_H
