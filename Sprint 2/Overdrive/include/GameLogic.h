#ifndef GAMELOGIC_H
#define GAMELOGIC_H

#include <SFML/Graphics.hpp>
#define GLEW_STATC
#include <GL/glew.h>
#include <iostream>
#include <time.h>
/***Extern class***/
#include "CreateShader.h"
#include "Camera.h"
#include "Track.h"
#include "SkyBox.h"
#include "MakeQuad.h"
#include "Vehicle.h"
#include "Texte.h"
/***OpenGL Maths***/
#include <glm/ext.hpp>


class GameLogic
{
    public:
        GameLogic();
        void event();
        void update();
        void draw();
        void mainLoop();
        void run();

    protected:

    private:
        sf::Clock clock;
        sf::Clock timer;
        sf::Time seconds;
        /***Camera***/
        Camera myCam;

        /*Viewport*/
        sf::ContextSettings settings;

        sf::RenderWindow window;

        Track muteCity;
        Texte laps;
        Vehicle blueFalcon;
        SkyBox skyBox;
        Hud hud;

        /***** BUFFERS *****/
        //Skybox
        int sb_arrayId;
        int sb_texHandler;

        /***Avec la class Shader***/
        CreateShader skyBoxShader;

        /***Autres variables***/
        bool running{true};



};

#endif // GAMELOGIC_H
