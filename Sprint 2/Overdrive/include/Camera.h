#ifndef CAMERA_H
#define CAMERA_H

#include <SFML/Graphics.hpp>
#define GLEW_STATC
#include <GL/glew.h>
#include <iostream>
#include <math.h>
/***openGL Maths***/
#include <glm/ext.hpp>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>


class Camera
{
    public:
        Camera(glm::vec3 camPos, glm::vec3 camFront, glm::vec3 camUp, float speed, float pitch, float yaw, float intensity);
        void moveCam();
        void updateCam();

        /*View & Projection Matrix*/
        glm::mat4 getModel();
        glm::mat4 getView();
        glm::mat4 getProjection();

        void setModel(glm::mat4 model);
        void setView(glm::mat4 view);
        void setProjection(glm::mat4 projection);


        /*Getters & Setters*/
        glm::vec3 getCameraPos();
        glm::vec3 getCameraFront();
        glm::vec3 getCameraUp();
        glm::vec3 getCameraRight();
        float getSpeed();
        float getPitch();
        float getYaw();
        float getIntensity();
        glm::mat4 getViewMat();

        void setCameraPos(glm::vec3 n_cameraPos);
        void setCameraFront(glm::vec3 n_cameraFront);
        void setCameraUp(glm::vec3 n_cameraUp);
        void setCameraRight(glm::vec3 n_cameraRight);
        void setSpeed(float n_speed);
        void setPitch(float n_pitch);
        void setYaw(float n_yaw);
        void setIntensity(float n_value);

        /***Bool d'actions***/
        bool getAccelerating();
        bool getReversing();
        bool getTurningLeft();
        bool getTurningRight();
        bool getDrifting();

        /***Controller inputs***/
        int controllerID{0};
        int A{0};
        int B{1};
        int X{3};
        int Y{4};
        int R{6};
        int L{7};
        int UP, RIGHT{100};
        int DOWN, LEFT{-100};


    private:
        glm::vec3 cameraPos;
        glm::vec3 cameraFront;
        glm::vec3 cameraUp;
        glm::vec3 cameraRight;
        glm::mat4 view;
        glm::mat4 projection; //perspective (FoV (zoom), Aspect ratio, Near, Far)
        glm::mat4 model;
        float speed;
        float pitch;
        float yaw;
        float intensity;
        bool isAccelerating{false};
        bool isReversing{false};
        bool isTurningLeft{false};
        bool isTurningRight{false};
        bool isDrifting{false};

};

#endif // CAMERA_H
