#ifndef BILLBOARD_H
#define BILLBOARD_H

#include <SFML/Graphics.hpp>
#define GLEW_STATC
#include <GL/glew.h>
#include <iostream>
#include <math.h>
#include <string.h>
/***extern class***/
#include "CreateShader.h"
#include "Camera.h"
#include "MakeQuad.h"

/***OpenGL Maths***/
#include <glm/ext.hpp>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>


class Billboard
{
    public:
        Billboard();
        glm::mat4 getModel();
        void makeBillboard(CreateShader &shader, Camera *cam, glm::vec3 position, float rotation, float scale);
        void orthoBillboard(CreateShader &shader, Camera *cam, glm::vec3 position, float rotation, float scale);


    protected:

    private:
        glm::mat4 model;
};

#endif // BILLBOARD_H
