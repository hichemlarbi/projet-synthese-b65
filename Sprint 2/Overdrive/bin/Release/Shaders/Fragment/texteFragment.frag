#version 330 core

out vec4 FragColor; //variable to set color
in vec3 myColor; //Get color from vertex shader

in vec2 TexCoord;

//uniform vec4 myColor;
uniform sampler2D ourTexture;

uniform vec2 pos;
uniform vec2 size;

void main()
{
    //FragColor = vec4(myColor, 1.0); //Set the color to the color we got from the vertex shader
    FragColor = texture(ourTexture, pos + (TexCoord * size));


}
