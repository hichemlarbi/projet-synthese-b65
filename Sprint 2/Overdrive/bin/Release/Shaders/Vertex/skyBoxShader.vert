#version 330 core
layout (location = 0) in vec3 aPos; //get position

out vec3 TexCoords; //output texture to fragment shader

//3D Stuff...
uniform mat4 projection;
uniform mat4 vue;

void main()
{
    TexCoords = aPos;
    gl_Position = projection * vue * vec4(aPos, 1.0); //lire la multiplication a l'envers
}
